# Gothic Texture Check CLI

Simple command line tool which checks folders for `.tga` files and report if there are any naming errors. :100: 

### Description

Gothic game textures require specific naming in order to be loaded properly.

The biggest concern are the textures describing variations, such as `HUM_HEAD_V0_C0`.
In order to use texture `HUM_HEAD_V2_C1`, there must exist all the textures of lower order.

> `HUM_HEAD_V0_C0`
> `HUM_HEAD_V1_C0`
> `HUM_HEAD_V2_C0`
> `HUM_HEAD_V0_C1`
> `HUM_HEAD_V1_C1`
> `HUM_HEAD_V2_C1`
> etc.

### Features

* Find duplicates
* dicover missing texture variants
* discover missing animated texture frames

### Installation & Usage

Just extract .exe in your **Textures** folder and launch.

**Download:**
[GothicTextureCheck.7z](/uploads/a7283fddd9b5ef0fbb636b801e53a5d8/GothicTextureCheck.7z)